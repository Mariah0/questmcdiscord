package dev.mariah.questmc.bot;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;

import javax.security.auth.login.LoginException;

public class Bot {

    private final String token;

    private JDA jda;

    public Bot(String token) {
        this.token = token;
        login();
    }
    private void login() {
        try {
            jda = new JDABuilder().setToken(token).setBulkDeleteSplittingEnabled(false).build();
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }
    public JDA getJda() {
        return jda;
    }
}
