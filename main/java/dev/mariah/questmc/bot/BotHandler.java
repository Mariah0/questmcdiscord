package dev.mariah.questmc.bot;

import dev.mariah.questmc.DiscordSync;
import dev.mariah.questmc.listeners.DiscordListener;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.*;

public class BotHandler {

    private final Bot bot;

    private Guild guild;

    private Role unverified;
    private Role verified;

    public BotHandler(DiscordSync discordSync) {
        bot = new Bot("NjEyNzYxMjQwMDQ5MDI1MDQ1.XVnEwA.Cc5ko7mzmlDLKT5i1HyooclQxPQ");
        bot.getJda().getPresence().setPresence(OnlineStatus.ONLINE, Activity.playing("play.quest-mc.com"));
        bot.getJda().addEventListener(new DiscordListener(discordSync, this));

        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(2000);
                    guild = bot.getJda().getGuildById(605483773626286239L);

                    unverified = guild.getRolesByName("Unverified", true).get(0);
                    verified = guild.getRolesByName("Verified", true).get(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
    public boolean isCorrectGuild(Guild guild) {
        return guild.getId().equals(this.guild.getId());
    }
    public void giveUnverified(User user) {
        Member member = guild.getMember(user);
        if (member.getRoles().contains(unverified)) {
            return;
        }
        guild.addRoleToMember(member, unverified).queue();
    }
    public void giveVerified(User user) {
        Member member = guild.getMember(user);
        if (member.getRoles().contains(verified)) {
            return;
        }
        guild.addRoleToMember(member, verified).queue();
    }
    public void removeUnverified(User user) {
        Member member = guild.getMember(user);
        if (!member.getRoles().contains(unverified)) {
            return;
        }
        guild.removeRoleFromMember(member, unverified).queue();
    }
    public void removeVerified(User user) {
        Member member = guild.getMember(user);
        if (!member.getRoles().contains(verified)) {
            return;
        }
        guild.removeRoleFromMember(member, verified).queue();
    }
    public User getUser(String id) {
        return bot.getJda().getUserById(id);
    }
}
