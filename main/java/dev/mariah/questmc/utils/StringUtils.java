package dev.mariah.questmc.utils;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class StringUtils {

    private int count;
    private final Random random;

    private final char[] chars = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

    public StringUtils() {
        random = ThreadLocalRandom.current();
    }
    public String generateRandomString(int length) {
        String result = "";

        for (int i = 0; i < length; i++) {
            char c = chars[random.nextInt(chars.length)];

            if (random.nextBoolean()) {
                c = Character.toUpperCase(c);
            }
            result += c;
        }
        return result + count++;
    }
}
