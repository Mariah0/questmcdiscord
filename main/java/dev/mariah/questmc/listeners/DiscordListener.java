package dev.mariah.questmc.listeners;

import dev.mariah.questmc.DiscordSync;
import dev.mariah.questmc.bot.BotHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.bukkit.entity.Player;

import java.awt.*;

public class DiscordListener extends ListenerAdapter {

    private final DiscordSync discordSync;
    private final BotHandler botHandler;

    private final Color color = Color.decode("#c634eb");

    private final MessageEmbed error_invalid_id;
    private final MessageEmbed error_unknown;

    public DiscordListener(DiscordSync discordSync, BotHandler botHandler) {
        this.discordSync = discordSync;
        this.botHandler = botHandler;

        error_invalid_id = new EmbedBuilder().setColor(color).addField("**Error**", "Invalid ID!", true).build();
        error_unknown = new EmbedBuilder().setColor(color).addField("**Error**", "Please contact Mariah#2923!", true).build();
    }
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        MessageChannel channel = event.getChannel();
        if (!channel.getType().equals(ChannelType.PRIVATE)) {
            return;
        }
        Message message = event.getMessage();
        String content = message.getContentRaw();
        if (!content.toUpperCase().startsWith("!LINKACCOUNT")) {
            return;
        }
        String id = content.substring(13);

        if (!discordSync.isValidID(id)) {
            channel.sendMessage(error_invalid_id).queue();
            return;
        }
        boolean completed = discordSync.useID(id, event.getAuthor());
        if (!completed) {
            channel.sendMessage(error_unknown).queue();
            return;
        }
        sendSuccessMessage(channel, discordSync.getPlayer(event.getAuthor().getId()));
    }
    private void sendSuccessMessage(MessageChannel channel, Player player) {
        MessageEmbed message = new EmbedBuilder().setColor(color).addField("**Accounts linked!**", "Your discord account has been linked to **" + player.getName() + "**!", true).build();
        channel.sendMessage(message).queue();
    }
    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        if (!botHandler.isCorrectGuild(event.getGuild())) {
            return;
        }
        botHandler.giveUnverified(event.getUser());
    }
}
