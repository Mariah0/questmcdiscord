package dev.mariah.questmc.listeners;

import dev.mariah.questmc.DataHandler;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class LoginListener implements Listener {

    private final DataHandler dataHandler;

    public LoginListener(DataHandler dataHandler) {
        this.dataHandler = dataHandler;
    }
    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        dataHandler.loadPlayer(event.getPlayer());
    }
}
