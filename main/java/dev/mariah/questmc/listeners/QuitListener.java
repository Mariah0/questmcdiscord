package dev.mariah.questmc.listeners;

import dev.mariah.questmc.DataHandler;
import dev.mariah.questmc.DiscordSync;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitListener implements Listener {

    private final DiscordSync discordSync;
    private final DataHandler dataHandler;

    public QuitListener(DiscordSync discordSync, DataHandler dataHandler) {
        this.discordSync = discordSync;
        this.dataHandler = dataHandler;
    }
    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        discordSync.autoRemove(event.getPlayer());
        dataHandler.savePlayer(event.getPlayer());
    }
}
