package dev.mariah.questmc;

import dev.mariah.questmc.bot.BotHandler;
import dev.mariah.questmc.commands.DLCommand;
import dev.mariah.questmc.commands.ULCommand;
import dev.mariah.questmc.listeners.LoginListener;
import dev.mariah.questmc.listeners.QuitListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class QuestMCDiscord extends JavaPlugin {

    private DiscordSync discordSync;
    private BotHandler botHandler;
    private DataHandler dataHandler;

    @Override
    public void onEnable() {
        discordSync = new DiscordSync(this);
        botHandler = new BotHandler(discordSync);
        dataHandler = new DataHandler(this);

        Bukkit.getPluginManager().registerEvents(new QuitListener(discordSync, dataHandler), this);
        Bukkit.getPluginManager().registerEvents(new LoginListener(dataHandler), this);

        getCommand("DL").setExecutor(new DLCommand(discordSync));
        getCommand("DiscordLink").setExecutor(new DLCommand(discordSync));
        getCommand("UL").setExecutor(new ULCommand(dataHandler, botHandler));
        getCommand("Unlink").setExecutor(new ULCommand(dataHandler, botHandler));
    }
    @Override
    public void onDisable() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            dataHandler.savePlayer(player);
        }
    }
    public DiscordSync getDiscordSync() {
        return discordSync;
    }
    public BotHandler getBotHandler() {
        return botHandler;
    }
    public DataHandler getDataHandler() {
        return dataHandler;
    }
}
