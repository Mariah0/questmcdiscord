package dev.mariah.questmc.commands;

import dev.mariah.questmc.DiscordSync;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DLCommand implements CommandExecutor {

    private final DiscordSync discordSync;

    public DLCommand(DiscordSync discordSync) {
        this.discordSync = discordSync;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only players can do this!");
            return false;
        }
        Player player = (Player) sender;
        if (discordSync.hasDiscordAccountLinked(player)) {
            sender.sendMessage(ChatColor.RED + "You already have a discord account linked!");
            return false;
        }
        String id = discordSync.getID(player);

        if (id == null) {
            id = discordSync.generateID(player);
        }
        sender.sendMessage(ChatColor.DARK_PURPLE + "QuestMC > " + ChatColor.GRAY + "Your code is " + ChatColor.DARK_PURPLE + id + "\n" +
                ChatColor.DARK_PURPLE + "QuestMC > " + ChatColor.GRAY + "Use it by sending !LinkAccount (ID) to our discord bot!");
        return true;
    }
}
