package dev.mariah.questmc.commands;

import dev.mariah.questmc.DataHandler;
import dev.mariah.questmc.bot.BotHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ULCommand implements CommandExecutor {

    private final DataHandler dataHandler;
    private final BotHandler botHandler;

    public ULCommand(DataHandler dataHandler, BotHandler botHandler) {
        this.dataHandler = dataHandler;
        this.botHandler = botHandler;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("questmc.discord.unlink")) {
            sender.sendMessage(ChatColor.RED + "You don't have permissions to do this!");
            return false;
        }
        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Invalid command usage:\n" +
                    ChatColor.RED + "/" + cmd.getName() + " (Player)");
            return false;
        }
        Player player = Bukkit.getPlayer(args[0]);
        if (player == null) {
            OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);
            if (!dataHandler.hasOfflineData(p)) {
                sender.sendMessage(ChatColor.RED + "That player has no discord account linked!");
                return false;
            }
            dataHandler.unlinkOffline(p);
            sender.sendMessage(ChatColor.DARK_PURPLE + "QuestMC > " + ChatColor.GRAY + "MC Account " + ChatColor.DARK_PURPLE + p.getName() + ChatColor.GRAY + " has been unlinked with their discord account!");
            return true;
        }
        if (!dataHandler.hasDiscordAccountLinked(player)) {
            sender.sendMessage(ChatColor.RED + "That player has no discord account linked!");
            return false;
        }
        dataHandler.unlinkOnline(player);
        sender.sendMessage(ChatColor.DARK_PURPLE + "QuestMC > " + ChatColor.GRAY + "MC Account " + ChatColor.DARK_PURPLE + player.getName() + ChatColor.GRAY + " has been unlinked with their discord account!");
        return true;
    }
}
