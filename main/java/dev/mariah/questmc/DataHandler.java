package dev.mariah.questmc;

import dev.mariah.questmc.utils.FileUtils;
import net.dv8tion.jda.api.entities.User;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DataHandler {

    private final File directory;

    private final Map<UUID, String> discordData;

    private final FileUtils fileUtils;
    private final QuestMCDiscord plugin;

    public DataHandler(QuestMCDiscord plugin) {
        this.plugin = plugin;
        directory = new File(plugin.getDataFolder(), "data");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        discordData = new HashMap<>();

        fileUtils = new FileUtils();
    }
    private File getDataFile(UUID uuid) {
        return new File(directory, uuid.toString() + ".yml");
    }
    public void loadPlayer(Player player) {
        UUID uuid = player.getUniqueId();
        File file = getDataFile(uuid);
        if (!file.exists()) {
            return;
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        String id = config.getString("DiscordID");

        discordData.put(uuid, id);
    }
    public void savePlayer(Player player) {
        UUID uuid = player.getUniqueId();
        if (!discordData.containsKey(uuid)) {
            return;
        }
        File file = getDataFile(uuid);
        if (!file.exists()) {
            fileUtils.createFile(file);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        config.set("DiscordID", discordData.remove(uuid));

        fileUtils.saveFile(file, config);
    }
    public void setAccount(UUID uuid, String id) {
        discordData.put(uuid, id);
    }
    public Player getPlayer(String id) {
        for (UUID uuid : discordData.keySet()) {
            if (!discordData.get(uuid).equals(id)) {
                continue;
            }
            return Bukkit.getPlayer(uuid);
        }
        return null;
    }
    public boolean hasDiscordAccountLinked(Player player) {
        return discordData.containsKey(player.getUniqueId());
    }
    public boolean hasOfflineData(OfflinePlayer player) {
        UUID uuid = player.getUniqueId();
        File file = getDataFile(uuid);
        return file.exists();
    }
    public void unlinkOnline(Player player) {
        UUID uuid = player.getUniqueId();
        if (discordData.containsKey(uuid)) {
            String id = discordData.remove(uuid);
            User user = plugin.getBotHandler().getUser(id);
            if (user != null) {
                plugin.getBotHandler().removeVerified(user);
                plugin.getBotHandler().giveUnverified(user);
            }
        }
        File file = getDataFile(uuid);
        if (!file.exists()) {
            return;
        }
        fileUtils.deleteFile(file);
    }
    public void unlinkOffline(OfflinePlayer player) {
        UUID uuid = player.getUniqueId();
        File file = getDataFile(uuid);
        if (!file.exists()) {
            return;
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        String id = config.getString("DiscordID");

        fileUtils.deleteFile(file);

        User user = plugin.getBotHandler().getUser(id);
        if (user != null) {
            plugin.getBotHandler().removeVerified(user);
            plugin.getBotHandler().giveUnverified(user);
        }
    }
}
