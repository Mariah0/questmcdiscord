package dev.mariah.questmc;

import dev.mariah.questmc.utils.StringUtils;
import net.dv8tion.jda.api.entities.User;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class DiscordSync {

    private final Random random;

    private final Map<UUID, String> ids;

    private final QuestMCDiscord plugin;
    private final StringUtils stringUtils;

    public DiscordSync(QuestMCDiscord plugin) {
        this.plugin = plugin;

        stringUtils = new StringUtils();
        random = ThreadLocalRandom.current();
        ids = new HashMap<>();
    }
    public String generateID(Player player) {
        String id = stringUtils.generateRandomString(5);
        ids.put(player.getUniqueId(), id);

        return id;
    }
    public String getID(Player player) {
        return ids.getOrDefault(player.getUniqueId(), null);
    }
    public void autoRemove(Player player) {
        UUID uuid = player.getUniqueId();
        if (ids.containsKey(uuid)) {
            ids.remove(uuid);
        }
    }
    public boolean isValidID(String id) {
        for (String s : ids.values()) {
            if (!s.equals(id)) {
                continue;
            }
            return true;
        }
        return false;
    }
    public boolean hasDiscordAccountLinked(Player player) {
        return plugin.getDataHandler().hasDiscordAccountLinked(player);
    }
    public boolean useID(String id, User user) {
        UUID uuid = retrieveUUID(id);

        if (uuid == null) {
            return false;
        }
        plugin.getDataHandler().setAccount(uuid, user.getId());
        ids.remove(uuid);
        plugin.getBotHandler().removeUnverified(user);
        plugin.getBotHandler().giveVerified(user);
        return true;
    }
    private UUID retrieveUUID(String id) {
        for (UUID uuid : ids.keySet()) {
            if (!ids.get(uuid).equals(id)) {
                continue;
            }
            return uuid;
        }
        return null;
    }
    public Player getPlayer(String id) {
        return plugin.getDataHandler().getPlayer(id);
    }
}
